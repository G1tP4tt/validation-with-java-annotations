package de.dhbw.vs.jprakt.validate;


public class Pair <T,T2> {
	
	private  T val1 ;
	private  T2 val2 ;
	
	public Pair(T arg0,T2 arg1) {
		setVal1(arg0);
		setVal2(arg1);
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public T getVal1() {
		return val1;
	}

	public T2 getVal2() {
		return val2;
	}

	public void setVal1(T val1) {
		this.val1 = val1;
	}

	public void setVal2(T2 val2) {
		this.val2 = val2;
	}


}

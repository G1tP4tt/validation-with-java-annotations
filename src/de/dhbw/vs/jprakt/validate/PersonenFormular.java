package de.dhbw.vs.jprakt.validate;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import javafx.scene.control.ComboBox;

public class PersonenFormular extends JPanel {
	
	JLabel name = new JLabel("Name");
	JLabel vorname = new JLabel("Vorname");
	JLabel eMail = new JLabel("E-Mail");
	JLabel anrede = new JLabel("Anrede");
	String[] gender = { "---", "Männlich", "Weiblich" };

	JLabel plz = new JLabel("PLZ");
	JLabel ort = new JLabel("Ort");

	@NotNull
	JTextField tfname = new JTextField();
	
	@NotNull
	JTextField tfvorname = new JTextField();
	
	@NotNull
	@Pattern(regexp = "^([_a-zA-Z0-9-]+(\\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*(\\.[a-zA-Z]{1,6}))?$", message = "Keine Valide Email")
	JTextField tfeMail = new JTextField();
	
	@NotNull
	@DecimalMin("3")
	JTextField tfplz = new JTextField();
	@NotNull
	JTextField tfort = new JTextField();

	public PersonenFormular() {
		super();

		this.setLayout(new GridLayout(7, 2));
		this.setPreferredSize(new Dimension(550, 350));

		JComboBox anredeBox = new JComboBox(gender);
		anredeBox.setSelectedIndex(0);

		this.add(anrede);
		this.add(anredeBox);

		this.add(name);
		this.add(tfname);

		this.add(vorname);
		this.add(tfvorname);

		this.add(eMail);
		this.add(tfeMail);

		this.add(plz);
		this.add(tfplz);

		this.add(ort);
		this.add(tfort);

		this.add(eMail);
		this.add(tfeMail);

	}

	public static void clearFormular() {
		
	}
}

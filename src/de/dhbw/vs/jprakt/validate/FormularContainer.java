package de.dhbw.vs.jprakt.validate;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.omg.Messaging.SyncScopeHelper;

import javax.validation.ConstraintViolation;

public class FormularContainer extends JFrame implements ActionListener {

	JPanel btnPanel = new JPanel();
	PersonenFormular personenFormular = new PersonenFormular();

	public FormularContainer() {
		super();

		JButton save = new JButton("Save");
		FormValidator validator = new FormValidator();
		save.addActionListener(this);

		btnPanel.add(personenFormular);
		btnPanel.add(save);

		this.add(btnPanel);

	}

	public static void main(String[] args) {
		FormularContainer formular = new FormularContainer();

		formular.setDefaultCloseOperation(EXIT_ON_CLOSE);
		formular.setSize(600, 500);
		formular.setVisible(true);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		PersonenFormular.clearFormular();

		FormValidator validator = new FormValidator();
		List<Pair<String, JTextField>> violations;

		try {
			violations = validator.validate(personenFormular);

			String message = "";

			for (Pair<String, JTextField> violation : violations) {
				violation.getVal2().setBackground(Color.RED);
				message += violation.getVal1() + "\n";
			}

			if (!violations.isEmpty()) {
				JOptionPane.showMessageDialog(this, message);
			}

		} catch (IllegalArgumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

}

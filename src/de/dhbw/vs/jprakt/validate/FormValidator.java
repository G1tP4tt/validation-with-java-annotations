package de.dhbw.vs.jprakt.validate;

import java.awt.Container;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.JComponent;
import javax.swing.JTextField;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class FormValidator {

	public FormValidator() {
		// TODO Auto-generated constructor stub
	}

	public List<Pair<String, JTextField>> validate(JComponent component) throws IllegalArgumentException, IllegalAccessException {

		List<Pair<String, JTextField>> violations = new ArrayList<>();
		
		List<Field> notNullListe = 
				Arrays.asList(component.getClass()
				.getDeclaredFields())
				.stream()
				.filter(c -> c.isAnnotationPresent(NotNull.class))
				.collect(Collectors.toList());
		List<Field> patternListe =
				Arrays.asList(component.getClass()
						.getDeclaredFields())
				.stream()
				.filter(c -> c.isAnnotationPresent(Pattern.class))
				.collect(Collectors.toList());
		

		for(Field f: notNullListe){
			
			JTextField textField = (JTextField) f.get(component);
			
			if(textField==null||textField.getText().equals("")) {
				
				String message = f.getAnnotation(NotNull.class).message();
				violations.add(new Pair<String,JTextField>(message,textField));
			}
			
		}
		
		for(Field f: patternListe){
			JTextField textField = (JTextField) f.get(component);
			Pattern p = f.getAnnotation(Pattern.class);
			
			
			if(!textField.getText().matches(p.regexp())){
				String message = f.getAnnotation(Pattern.class).message();
				violations.add(new Pair<String,JTextField>(message,textField));
			}
		}
		return violations;

	}

}
